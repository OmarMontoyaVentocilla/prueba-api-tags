
## Instrucciones

Para levantar la aplicacion, realizada en Laravel 8 necesitaremos las siguientes instrucciones:

- Tener instalado composer y  correr "composer install" para instalar las dependencias.
- Tener una base de datos creada y colocar "php artisan migrate" para correr las migraciones.
- Colocar el siguiente comando: "php artisan jwt:secret" para generar una key secreta ya que estamos trabajando con JWT.
- Para arrancar el servidor poner "php artisan serve".
- Utilizar las apis brindadas en la coleccion de postman.


