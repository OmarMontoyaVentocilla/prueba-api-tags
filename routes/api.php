<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\VideoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['jwt.verify'],'prefix' => 'v1/auth'], 

function($router) {

   
    Route::get('information', [AuthController::class, 'info'])->name('information.info');
    Route::get('me', [AuthController::class, 'test'])->name('me.test');

    //POST-USER
    Route::group(['prefix' => 'posts'],function($router){
        Route::get('/', [PostController::class, 'index'])->name('posts.index');
        Route::post('/', [PostController::class, 'store'])->name('posts.store');
    });

    //VIDEO-USER
    Route::group(['prefix' => 'video'],function($router){
        Route::get('/', [VideoController::class, 'index'])->name('video.index');
        Route::post('/', [VideoController::class, 'store'])->name('video.store');
    });

   
});

Route::post('login', [AuthController::class, 'login'])->name('login.login');
Route::post('register', [AuthController::class, 'store'])->name('register.store');

