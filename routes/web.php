<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json([
        "code" => 401,
        "userMessage" => "Token not send",
    ]);
});



// Route::group([
//     'middleware' => 'api'

// ], function ($router) {
//     Route::post('login', [\App\Http\Controllers\Api\V1\AuthController::class, 'login'])->name('login');
//     Route::post('logout', [\App\Http\Controllers\Api\V1\AuthController::class, 'logout'])->name('logout');
//     Route::post('refresh', [\App\Http\Controllers\Api\V1\AuthController::class, 'refresh'])->name('refresh');
//     Route::post('me', [\App\Http\Controllers\Api\V1\AuthController::class, 'me'])->name('me');
// });

// Route::group(['middleware' => 'api'], function ($router) {
   
// });

