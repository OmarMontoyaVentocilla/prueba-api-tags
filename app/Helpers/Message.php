<?php

namespace App\Helpers;

class Message
{
    public function __construct()
    {
        //construct
    }

    public function messageOk($devMessage, $code, $userMessage)
    {
        return [
            "devMessage" => $devMessage,
            "code" => $code,
            "userMessage" => $userMessage,
        ];
    }

    public function messageOkRegisterStyle1($devMessage, $userMessage, $type)
    {
        return  [
            "devMessage" => $devMessage,
            "userMessage" => $userMessage,
            "type" => $type
        ];
    }


    public function messageOkRegisterStyle2($validate, $msg, $code)
    {
        return  [
            "validate" => $validate,
            "msg" => $msg,
            "code" => $code
        ];
    }
}
