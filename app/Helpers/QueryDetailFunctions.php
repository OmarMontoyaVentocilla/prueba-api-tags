<?php

namespace App\Helpers;

class QueryDetailFunctions
{
    public function __construct()
    {
        //construct
    }

        //USUARIO
        public function respuestaOkUser($response)
        {
            $arregl = [];
            foreach ($response as $rp) {
                $dt = [
                    'id' => $rp['id'],
                    'email' => $rp['email'],
                    'name' => $rp['name']
                ];
                array_push($arregl, $dt);
            }
    
            return $arregl;
        }
    
}
