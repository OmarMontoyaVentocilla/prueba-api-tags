<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Message;
use App\Helpers\QueryDetailFunctions;
use Tymon\JWTAuth\Exceptions\JWTException;



class AuthController extends Controller
{
    protected $jwt;

    public function __construct(JWTAuth $jwt) {
        
        
        $this->jwt = $jwt;
    }

    public function getDataUser($email)
    {
        $queryFunctions = new QueryDetailFunctions;
        $response = User::where('email', $email)->where('state', 1)->get();
        return $queryFunctions->respuestaOkUser($response);
    }


    public function login(Request $request)
    {
        $message = new Message;

        $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        try {
            $usuario = $this->getDataUser(request('email'));
            $token = $this->jwt->claims(['user' => $usuario])->attempt($request->only('email', 'password'));
            if (!$token) {
                return response()->json($message->messageOk("User not found", 404, "Usuario no encontrado"));
            }
        } catch (JWTException $e) {

            return response()->json($message->messageOk($e->getMessage(), 500, "Token expirado"));
        }

        $this->jwt->setToken($token);
        return $this->respondWithToken($token);
    }

    public function refresh()
    {
        return $this->respondWithToken($this->jwt->refresh());
    }

    public function logout(Request $request)
    {
        auth()->logout(true);
        Auth::invalidate();
        $token = $this->jwt->getToken();
        $this->jwt->setToken($token)->invalidate(true);
        $this->jwt->invalidate();
        $this->jwt->invalidate(true);
        $this->jwt->invalidate($this->jwt->getToken());
        $this->jwt->invalidate($this->jwt->parseToken());
        $this->jwt->parseToken()->invalidate();
        return response()->json(['message' => 'Token removido']);
    }

    public function store(Request $request)
    {
        $message = new Message;
        $rules = [
            'email' => 'required',
            'name' => 'required',
            'password' => 'required',
        ];

        $this->validate($request, $rules);

        User::create(
            [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'state' => 1,
                'password' => Hash::make($request->input('password'))
            ]
        );
        return response()->json($message->messageOk("creado", "Se registró exitosamente", true));
    }



    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' =>  $this->jwt->factory()->getTTL() * 60
        ]);
    }

    public function test()
    {
        $data=["name"=>"data"];
        return response()->json($data);
    }

    public function info()
    {
        $data=["name"=>"my info"];
        return response()->json($data);
    }


}
