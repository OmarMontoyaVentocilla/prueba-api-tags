<?php

namespace App\Http\Controllers;

use App\Helpers\Message;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    use ApiResponser;
    
    public function index(Request $request)
    {
        $id = Auth::id();
        $post = Post::with('tagged')->where('user_id',$id)->get(); 
        return $this->successResponse($post);
    }


    public function store(Request $request)
    {
        $message = new Message;
        $tags = explode(',',$request->tags);
        $posts= Post::create($request->all());
        $posts->tag($tags);

        return $this->successResponse(
            $message->messageOkRegisterStyle2("", "Post creado satisfactoriamente", 200),
            Response::HTTP_CREATED
        );

    }

}
