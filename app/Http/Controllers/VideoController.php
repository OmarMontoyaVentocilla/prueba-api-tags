<?php

namespace App\Http\Controllers;

use App\Helpers\Message;
use App\Models\Video;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    use ApiResponser;
    
    public function index(Request $request)
    {
        $id = Auth::id();
        $video = Video::with('tagged')->where('user_id',$id)->get(); 
        return $this->successResponse($video);
    }


    public function store(Request $request)
    {
        $message = new Message;
        $tags = explode(',',$request->tags);
        $videos= Video::create($request->all());
        $videos->tag($tags);

        return $this->successResponse(
            $message->messageOkRegisterStyle2("", "Video creado satisfactoriamente", 200),
            Response::HTTP_CREATED
        );

    }
}
