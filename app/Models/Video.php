<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;

class Video extends Model
{
    use Taggable;

    protected $fillable = [
        'title', 'body','user_id'
    ];
}
